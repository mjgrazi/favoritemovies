//
//  MasterViewController.h
//  MovieLists
//
//  Created by Michael Graziano on 3/20/15.
//  Copyright (c) 2015 Mike Graziano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end

