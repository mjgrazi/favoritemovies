//
//  DetailViewController.h
//  MovieLists
//
//  Created by Michael Graziano on 3/20/15.
//  Copyright (c) 2015 Mike Graziano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

