# MovieLists
MovieLists is an app that lets you search the Rotten Tomatoes to find and save your favorite movies. To begin, simply enter the name of your favorite movies into the search bar. Tap the movie poster to bring up details, and then tap the star in the top right to save the movie to your favorites list. MovieLists will save your favorite movies using Core Data.

## History

1.0: 3/23/2015 - Initial build

- Perform a search and display up to 12 top results
- View movie details including release date, Rotten Tomatoes critical aggregate ratings, and the leading cast
- Save your favorite movies as you search through the list
